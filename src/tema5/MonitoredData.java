package tema5;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MonitoredData {

	private Date startTime;
	private Date endTime;
	private String activityLabel;
	
	public MonitoredData(Date startTime, Date endTime, String activityLabel){
		this.startTime = startTime;
		this.endTime = endTime;
		this.activityLabel = activityLabel;
	}
	
	@Override
	public String toString(){
		return(startTime.toString() + " " + endTime.toString() + " " + activityLabel);
	}
	
	@Override 
	public int hashCode(){ 
		int hashVal = activityLabel.hashCode();
		return hashVal *2;
	}
	
	public Date getStartDate(){
		return this.startTime;
	}
	
	public Date getEndDate(){
		return this.endTime;	
	}
	
	public String getLabel(){
		return this.activityLabel;
	}
	
	public int getDay(){
		 Calendar calendar = new GregorianCalendar();
		 calendar.setTime(startTime);
		 return calendar.get(Calendar.DAY_OF_YEAR);
	}
	
	@SuppressWarnings("deprecation")
	public long calculateDurationHours() { 
		long difference = endTime.getTime() - startTime.getTime();
		Time time = new Time(difference);
		return time.getHours();
	};
	
	public long calculateDurationMinutes() {
		long difference = endTime.getTime() - startTime.getTime();
		Time time = new Time(difference);
		return time.getMinutes();
	}
	
	public long getDuration(){ 
		return endTime.getTime() - startTime.getTime();
	};
}
