package tema5;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Reader {

	private String fileName;
	
	public Reader(String fileName){ 
		this.fileName = fileName;
	}
	
	public List<MonitoredData> readFile(){
		List<MonitoredData> data = new ArrayList<MonitoredData>();
		String[] split = null;
		String s = null;
		
		try {
			FileReader reader = new FileReader(this.fileName);
			@SuppressWarnings("resource")
			BufferedReader buffreader = new BufferedReader(reader);
			while ((s = buffreader.readLine()) != null){
				split = s.split("[\t ]+");
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
				Date startDate = dateFormat.parse(split[0] + " " + split[1]);
				Date endDate = dateFormat.parse(split[2] + " " + split[3]);
				data.add(new MonitoredData(startDate, endDate, split[4]));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
	
}
