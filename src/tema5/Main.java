package tema5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


public class Main {
	static StringBuilder sb = new StringBuilder();
	private static List<MonitoredData> data = new ArrayList<MonitoredData>();
	static long differentDay = 1;
	static Map<String, Long> occurences = new HashMap<String, Long>();	
	static Map<Integer, HashMap<String, Long>> occPerDay = new HashMap<Integer, HashMap<String, Long>>();
	static Map<Integer, List<String>> activities = new HashMap<Integer, List<String>>();
	static HashMap<String, Long> emptyMap = new HashMap<String, Long>();
	static HashMap<String, Long> activityDuration = new HashMap<String, Long>();
	static HashMap<String, Long> activityDurationFinal = new HashMap<String, Long>();
	static HashMap<String, Long> underFive = new HashMap<String, Long>();
	static int days = 1;
	
	public static void main(String[] args){
		Reader reader = new Reader("Activities.txt");
		data = reader.readFile();
		
		Lambda calculatorDifferentDays = (d1, d2) -> {
			long diff = d2.getTime() - d1.getTime();
			return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		};
	
		differentDay = calculatorDifferentDays.getDifference(data.get(0).getStartDate(),data.get(data.size()-1).getEndDate());		
		System.out.println(differentDay);	
		occurences = data.stream().collect(Collectors.groupingBy(MonitoredData::getLabel, Collectors.counting()));
		activities = data.stream().collect(Collectors.groupingBy(MonitoredData::getDay, Collectors.mapping(MonitoredData::getLabel, Collectors.toList())));
		for(Integer i: activities.keySet()){
			occPerDay.put(i, emptyMap);
			occPerDay.get(i).putAll(activities.get(i).stream().collect(Collectors.groupingBy(o->o.toString(), Collectors.counting())));
		}
		activityDuration = (HashMap<String, Long>) data.stream().collect(Collectors.groupingBy(MonitoredData::getLabel, Collectors.summingLong(MonitoredData::getDuration)));
		activityDurationFinal = (HashMap<String, Long>) activityDuration.entrySet().stream().filter(d -> {return d.getValue()>36000000;}).collect(Collectors.toMap(p->p.getKey(), p->p.getValue()));
		
		underFive = (HashMap<String, Long>) data.stream().filter(d -> { return d.calculateDurationMinutes() < 5;}).collect(Collectors.groupingBy(MonitoredData::getLabel, Collectors.counting()));
		underFive.forEach((x, y) -> {
			occurences.forEach((a, b) -> {
				if (x.equals(a)) 
					if (y>= 0.9f*b){
						sb.append(x); 
						sb.append(System.getProperty("line.separator"));
						}
				});
			});
		
		writeToFile(sb.toString(), "task5.txt");
		sb = new StringBuilder();
		
		for(String s: occurences.keySet()){
			sb.append(s + ": " + occurences.get(s).intValue());
			sb.append(System.getProperty("line.separator"));
		}
		
		writeToFile(sb.toString(), "task2.txt");
		sb = new StringBuilder();
		
		for(Integer i: occPerDay.keySet()){
			for(String s: occPerDay.get(i).keySet()){
				sb.append(i + " " + s + " " + occPerDay.get(i).get(s));
				sb.append(System.getProperty("line.separator"));
			}
		}
		
		writeToFile(sb.toString(), "task3.txt");
		sb = new StringBuilder();
		
		for (String s: activityDurationFinal.keySet()){
			long hour = (activityDurationFinal.get(s)/ (1000*60*60));
			long minute = (activityDurationFinal.get(s) / (1000 * 60)) % 60;
			long second = (activityDurationFinal.get(s) / 1000) % 60;
			sb.append(s + " : " + hour +":"+ minute + ":" + second);
			sb.append(System.getProperty("line.separator"));
		}
		
		writeToFile(sb.toString(), "task4.txt");
		sb = new StringBuilder();
		
	}
	
	public static void writeToFile(String s, String fileName){
		BufferedWriter bufferedWriter = null;
		FileWriter fileWriter = null; 
		
		try{ 
			fileWriter = new FileWriter(fileName);
			bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(s);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bufferedWriter.close();
				fileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public interface Lambda{
		long getDifference(Date date1, Date date2);
	}
}

